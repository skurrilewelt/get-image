<?php

/**
 * get-image main file
 * package get-image
 * @created 2016-10-01
 * @author: Olaf Sweekhorst http://skurrilewelt.de
 */
class GetimageMain
{
    protected $textdomain;
    protected $version;
    protected $plugin_slug;

    public function __construct($textdomain)
    {
        $this->plugin_slug = 'get-image';
        $this->version = '1.0.0';
        $this->setTextdomain($textdomain);

        $cpt = new GetimageCPT();
        $cpt->setTextdomain($textdomain);

        define('GETIMAGE_TEXTDOMAIN', $this->textdomain);
        load_plugin_textdomain($this->textdomain, false, dirname(plugin_basename(__FILE__)) . '/languages');

        // admin scripts will only called, if my admin page or view is loaded
        if (is_admin()) {

            add_action('admin_enqueue_scripts', array($this, 'addAdminScripts'));
            add_action('manage_posts_custom_column', array($cpt, 'getimageGetColumns'));
            add_filter('manage_edit-getimage-request_columns', array($cpt, 'getimageAddColumns'));
            add_action('admin_init', array($cpt, 'addMetaBox'));
            add_action('save_post', array($cpt, 'getimageSaveMeta'));
            //add_filter ("manage_edit-igreisen_columns", array( $this, 'igreisen_edit_columns'));
        }

        add_action('init', array($cpt, 'registerPosttype'));
        add_action('init', array($this, 'createEndpoint'));
        add_action('template_redirect', array($this, 'prepareAndSend'));

    }

    /**
     * Add rewrite rules to catch the image / post id
     */
    public function createEndpoint()
    {
        add_rewrite_tag('%get_id%', '([^&]+)');
        add_rewrite_rule('get/([^&]+)/?', 'index.php?get_id=$matches[1]', 'top');
    }

    /**
     * Get the post and the image url, prepare bas64 encoding and
     * deliver the image source with appropriate headers (see GetimageGet->imageFromBase65)
     *
     * @return bool
     */
    public function prepareAndSend()
    {
        global $wp_query;


        $get_id = $wp_query->get('name');
        if ('get_id' != $get_id) {
            return false;
        }

        $id = (int)$wp_query->get('page');

        if(0 === $id) {
            return false;
        }

        GetimageGet::saveCount($id);

        $post = get_post($id);

        $get = new GetimageGet($post->getimage_file);
        $get->imageFromBase65();
        wp_die();
    }


    public function addAdminScripts()
    {
        wp_enqueue_script('jquery');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('media-upload');
        wp_enqueue_media();
    }


    public function getVersion()
    {
        return $this->version;
    }

    public function setTextdomain($textdomain)
    {
        $this->textdomain = $textdomain;
    }
}