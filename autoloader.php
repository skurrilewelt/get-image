<?php

/**
 * Autoloading of gt-image classes
 * package get-image
 * @created 2016-10-01
 * @author: Olaf Sweekhorst http://skurrilewelt.de
 */
class GetimageAutoloader
{

    public static $autoloader = null;
    public static $path = null;

    public static function init()
    {
        if (self::$autoloader == null) {
            self::$autoloader = new self;
        }

        return self::$autoloader;
    }

    public function __construct()
    {
        self::$path = realpath(__DIR__);

        spl_autoload_register(array($this, 'autoload_cm'));
    }


    public function autoload_cm($class)
    {
        if ( false !== strpos( $class, 'Getimage' ) ) {
            $class2file = strtolower(str_replace('Getimage', 'Getimage-', $class));
            $paths = array(
                self::$path . '/' . $class2file . '.php',
                self::$path . '/views/' . $class2file . '.php',
                self::$path . '/classes/' . $class2file . '.php',
                self::$path . '/models/' . $class2file . '.php',
            );

            foreach ($paths as $path) {
                if (@file_exists($path)) {
                    include_once $path;
                    if (class_exists($class, false)) {
                        break;
                    }
                }
            }
        }
    }

}