<?php

/**
 *
 * @created 11.09.2016
 * @author: Olaf Sweekhorst <info@skurrilewelt.de>
 */
class GetimageGet
{

    private $imagebase;
    private $mime_type = 'image/jpeg';
    private $file_name;
    private $file_path;

    /**
     * GetimageGet constructor.
     * Collect base information, mime type and path from image urk
     * and create the base64 representation
     * @param $file_url
     */
    public function __construct($file_url)
    {
        $path = pathinfo($file_url);
        $parse = parse_url($file_url);
        $this->file_name = $path['basename'];
        $this->file_path = $_SERVER['DOCUMENT_ROOT'] . '/' . $parse['path'];
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $this->mime_type = finfo_file($finfo, $this->file_path);
        $this->imageBase = $this->imageToBase65();
    }

    private function imageToBase65()
    {
        $content = file_get_contents($this->file_path);
        return base64_encode($content);
    }

    /**
     * Send header and the base64 decoded image source.
     */
    public function imageFromBase65()
    {
        header('Content-type: ' . $this->mime_type);
        header('Content-Disposition: inline; filename=' . $this->file_name);
        echo base64_decode($this->imageBase);

    }

    /**
     * Get count meta value and calculate total and this month counts
     * @param $post_id
     * @return array
     */
    public static function getCounts($post_id)
    {
        $meta = get_post_meta($post_id);
        $result = array();
        $result['total'] = 0;
        $result['this_month'] = 0;

        if (!empty($meta['getimage_counts'])) {
            $counts = maybe_unserialize($meta['getimage_counts'][0]);
            $month = date('n');
            $year = date('Y');

            foreach ($counts as $date => $count) {
                $tmp = explode('-', $date);
                if ($tmp[0] == $year && $tmp[1] == $month) {
                    $result['this_month'] = $count;
                }
                $result['total'] += $count;
            }
        }


        return $result;
    }

    /**
     * Save a request to a certain post_id.
     * Save counts to year-month array key to make a minimal statistic possible.
     * @param $post_id
     */
    public static function saveCount($post_id)
    {
        $meta = get_post_meta($post_id);
        $month = date('n');
        $year = date('Y');
        $counts = maybe_unserialize($meta['getimage_counts'][0]);

        if(!is_array($counts)) {
            $counts = array();
        }
        $key = $year . '-' . $month;

        if (array_key_exists($key, $counts)) {
            $counts[$key]++;
        } else {
            $counts[$key] = 1;
        }

        update_post_meta($post_id, 'getimage_counts', $counts);
    }
}