<?php

/**
 * Post type for get-image
 * @created 10.09.2016
 * @author: Olaf Sweekhorst <info@skurrilewelt.de>
 * @version $END$
 */
class GetimageCPT
{

    protected $textdomain;

    public function registerPosttype()
    {
        $labels = array(
            'name' => __('Requests', $this->textdomain),
            'singular_name' => __('Request', $this->textdomain),
            'add_new' => __('Add New', $this->textdomain),
            'add_new_item' => __('Add New Request', $this->textdomain),
            'edit_item' => __('Edit Request', $this->textdomain),
            'new_item' => __('New Request', $this->textdomain),
            'view_item' => __('View Request', $this->textdomain),
            'search_items' => __('Search Request', $this->textdomain),
            'not_found' => __('No request found', $this->textdomain),
            'not_found_in_trash' => __('No request found in Trash', $this->textdomain),
            'parent_item_colon' => '',
        );

        $args = array(
            'label' => __('Requests', $this->textdomain),
            'labels' => $labels,
            'public' => true,
            'can_export' => true,
            'show_ui' => true,
            '_builtin' => false,
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-upload',
            'hierarchical' => false,
            'rewrite' => array("slug" => "getimages"),
            'supports' => array('title', 'thumbnail'),
            'show_in_nav_menus' => true,
            //'taxonomies' => array('tf_eventcategory', 'post_tag')
        );

        register_post_type('getimage-request', $args);
    }

    public function getimageAddColumns($columns)
    {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __('Location'),
            'getimage_col_file' => __('Filepath', $this->textdomain),
            'getimage_col_thumb' => __('Thumbnail'),
            'getimage_col_counts' => __('Counts Total / This Month', $this->textdomain),
            'date' => __('Date')
        );
        return $columns;
    }

    public function getimageGetColumns($column)
    {
        global $post;
        $custom = get_post_custom();
        switch ($column) {
            case 'getimage_col_file':
                echo $custom['getimage_file'][0];
                break;
            case 'getimage_col_thumb':
                $file = (!empty($custom['getimage_file'][0])) ? $custom['getimage_file'][0] : '';
                echo '<img src="' . $file .'" class="preview-upload" id="getimage_preview" width="100" />';
                break;
            case 'getimage_col_counts' :
                $counts = GetimageGet::getCounts($post->ID);
                echo  $counts['total'] . ' / ' . $counts['this_month'];
                break;

        }
    }

    public function addMetaBox()
    {
        add_meta_box('getimage_meta', __('Request', $this->textdomain), array($this, 'getImageMetaBox'),
            'getimage-request');
    }

    function getImageMetaBox()
    {
        global $post;
        wp_enqueue_style('getimage1', GETIMAGE_CSS . 'getimage_admin.css');
        wp_enqueue_script('getimage1', GETIMAGE_JS . 'getimage_admin.js', array('jquery'));
        $meta = get_post_custom($post->ID);
        $file = (!empty($meta['getimage_file'][0])) ? $meta['getimage_file'][0] : '';
        $target = (!empty($meta['getimage_target'][0])) ? $meta['getimage_target'][0] : '';
        $counts = (!empty($meta['getimage_counts'][0])) ? GetimageGet::getCounts($post->ID) : array('this_month' => 0, 'total' => 0);

        echo '<input type="hidden" name="nonce" id="nonce" value="' . wp_create_nonce('getimage-nonce') . '" />';

        ?>
        <div class="getimage_meta">
            <ul>
                <li><label><?php _e('Filepath', $this->textdomain); ?></label>
                    <input type="text" name="getimage_file" id="getimage_file" class="" value="<?php echo $file; ?>"/>
                    <input id="upload_image_button" type="button" class="upload_image_button button" value="<?php _e('Select Image', $this->textdomain); ?>" />
                <li class="preview-image"><label><?php _e('Preview', $this->textdomain); ?></label>
                    <img src="<?php echo $file; ?>" class="preview-upload" id="getimage_preview" width="200" />
                </li>
                <li><label><?php _e('Target Description', $this->textdomain); ?></label>
                    <input type="text" name="getimage_target" class="" value="<?php echo $target; ?>"/>
                </li>
                <li><label><?php _e('Counts', $this->textdomain); ?></label>
                    <span><?php echo __('This month', $this->textdomain) . ': ' . $counts['this_month']; ?></span>
                    <span><?php echo __('Total', $this->textdomain) . ': ' . $counts['total']; ?></span>
                </li>
                <li><label><?php _e('img tag', $this->textdomain); ?></label>
                    <input readonly type="text" name="" class=""
                    <?php
                    if(0 != $post->ID) {
                        echo 'value="' . htmlentities('<img src="http://wordpress.skurrilewelt.de/get_id/' . $post->ID . '" />') . '"';
                    } else {
                        echo 'value="' . __('Save first', $this->textdomain) . '"';
                    }
                    ?>/>
                </li>
            </ul>
        </div>
        <?php
    }

    public function getimageSaveMeta()
    {
        global $post;

        if (empty($post)) {
            return false;
        }

        if (isset($_POST['at_nonce']) && !wp_verify_nonce($_POST['nonce'], 'getimage-nonce')) {
            return $post->ID;
        }

        if(!isset($_POST['getimage_target']) || !isset($_POST['getimage_file'])) {
            return false;
        }

        if (!current_user_can('edit_post', $post->ID)) {
            return $post->ID;
        }
        update_post_meta($post->ID, 'getimage_target', $_POST['getimage_target']);
        update_post_meta($post->ID, 'getimage_file', $_POST['getimage_file']);
    }

    /**
     * @param mixed $textdomain
     */
    public function setTextdomain($textdomain)
    {
        $this->textdomain = $textdomain;
    }



}