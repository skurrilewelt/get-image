<?php
/*
 Plugin Name: GetImage

 Plugin URI: http://wordpress.skurrilewelt.de
 Description: Serve images with php
 Author: Olaf Sweekhorst
 Version: 1.0.0
 Text Domain: get-image
 Author URI: http://skurrilewelt.de
 */


if (!defined('ABSPATH')) {
    die;
}

define('GETIMAGE_IMAGES', plugin_dir_url(__FILE__) . 'assets/images/');
define('GETIMAGE_CSS', plugin_dir_url(__FILE__) . 'assets/css/');
define('GETIMAGE_JS', plugin_dir_url(__FILE__) . 'assets/js/');

require_once 'autoloader.php';
GetimageAutoloader::init();

$get_image = new GetimageMain('get-image');