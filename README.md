![alt tag](http://wordpress.skurrilewelt.de/get_id/487)
### What is this repository for? ###

This is a little WordPress plugin to serve images via php to external target pages as img src base64 content.
You can use it maybe as a tracker or as a kind of cdn with minimal usage statistics.

### How do I get set up? ###

* Clone to your plugin directory and active as usual OR
* Download and install from backend and activate.
* Your menu will show the item 'Requests'
* Create a new and copy the given image tag to any external site to display the image and track usage,

### Contribution guidelines ###

* Pull requests and suggestions are welcome

See hhttp://wordpress.skurrilewelt.de/free-plugin-get-image/ for further details